import { createApp, defineAsyncComponent } from 'vue';

import App from './App';
import router from './router';
import strore from './store/index';
// Components Globali
import BaseCard from './components/ui/BaseCard';
import BaseButton from './components/ui/BaseButton';
import BaseBadge from './components/ui/BaseBadge';
import BaseSpinner from './components/ui/BaseSpinner';

// Con defineAsyncComponent il component viene letto in lazyloading 
const BaseDialog = defineAsyncComponent(() => import('./components/ui/BaseDialog'));

const app = createApp(App);

app.use(router); // regitrazione del route
app.use(strore); // registrazione dello store

// Registrazione a livello globale dei components
app.component('base-card', BaseCard); 
app.component('base-button', BaseButton);
app.component('base-badge', BaseBadge);
app.component('base-spinner', BaseSpinner);
app.component('base-dialog', BaseDialog);

app.mount('#app');
