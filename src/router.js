import { createRouter, createWebHistory } from 'vue-router';

import store from './store/index';

import UserAuth from './pages/auth/UserAuth';
import CoachesList from './pages/coaches/CoachesList';
import NotFound from './pages/NotFound';

/* Non è raccomandato per il route usare il lazyloading si preferisce questa
   sintassi e leggere i component con delle condizioni tramite v-if */
const CoachDetail = () => import('./pages/coaches/CoachDetail');
const CoachRegistration = () => import('./pages/coaches/CoachRegistration');
const ContactCoach = () => import('./pages/requests/ContactCoach');
const RequestsReceived = () => import('./pages/requests/RequestsReceived');

const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: '/', redirect: '/coaches' },
    { path: '/coaches', component: CoachesList },
    {
      path: '/coaches/:id',
      component: CoachDetail,
      props: true, // impostando props a true, nel component si avrà a disposizione come props il parametro passato(id)
      children: [
        { path: 'contact', component: ContactCoach } // /coaches/c1/contact
      ]
    },
    { path: '/register', component: CoachRegistration, meta: { requiresAuth: true } }, // si collegano dei metadata con una chiave
    { path: '/requests', component: RequestsReceived, meta: { requiresAuth: true } },  // si collegano dei metadata con una chiave
    { path: '/auth', component: UserAuth, meta: { requiresUnauth: true } },  // si collegano dei metadata con una chiave
    { path: '/:notFound(.*)', component: NotFound }
  ]
});

// Si crea una Navigation Guard globale per controllare qualsiasi route
router.beforeEach(function(to, from , next) {
  // Si controlla il valore della chiave inserita nei metadata
  if (to.meta.requiresAuth && !store.getters.isAuthenticated) { // anche all'interno dei route si può accedere allo store
    next('/auth'); // autenticazione fallita si viene reindirizzati alla pagina di autenticazione
  } else if (to.meta.requiresUnauth && store.getters.isAuthenticated) {
    next('/coaches'); // si va alla pagina di autenticazione ma si è già autenticati, si va ai coaches
  } else {
    next(); // si consente la navigazione
  }
});

export default router;
