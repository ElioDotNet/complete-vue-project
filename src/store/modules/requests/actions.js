import axios from 'axios';

export default {
  async contactCoach(context, payload) {
    const newRequest = {
      coachId: payload.coachId,
      userEmail: payload.email,
      message: payload.message
    };
    const url = `${context.rootGetters.fireBaseUrl}/requests/${payload.coachId}.json`; // .json è richiesto da Firebase

    const response = await axios.post(url, {
        userEmail: payload.email,
        message: payload.message
      }
    );

    if (response.status !== 200) {
      throw new Error('Failed to send request');
    }
    newRequest.id = response.data.name;
    // Richiamo la mutation addRequest con la nuova request
    context.commit('addRequest', newRequest);
  },
  async fetchRequests(context) {
    const { userId: coachId, token, fireBaseUrl } = context.rootGetters;
    const url = `${fireBaseUrl}/requests/${coachId}.json?auth=${token}`; // auth serve per autenticazione
    const response = await axios.get(url);
    const { data } = response;

    if (response.status !== 200) {
      throw new Error('Fetch Requests Failed');
    }
    const requests = [];
    for (const key in data) {
      const request = {
        id: key,
        coachId,
        userEmail: data[key].userEmail,
        message: data[key].message,
      };
      requests.push(request); // elenco di tutte le requests

      context.commit('setRequests', requests);
    }
  }
};
