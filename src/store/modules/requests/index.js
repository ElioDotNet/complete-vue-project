import mutations from './mutations';
import actions from './actions';
import getters from './getters';

export default {
    namespaced: true, // si utilizzano i moduli
    state() {
        return {
            requests: []
        };
    },
    mutations,
    actions,
    getters
};