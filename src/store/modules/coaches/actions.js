import axios from 'axios';

export default {
  async addCoach(context, data) {
    const userId = context.rootGetters.userId; // rootGetters si riferisce ai getter globali
    const newCoach = {
      firstName: data.first,
      lastName: data.last,
      description: data.desc,
      hourlyRate: data.rate,
      areas: data.areas
    };

    const { token, fireBaseUrl } = context.rootGetters;
    const url = `${fireBaseUrl}/coaches/${userId}.json?auth=${token}`; // auth serve per l'autenticazione

    // Si fa una chiamata in Put a Firebase: se non esistono vengono creati, diversamenti vengono sovrascritti
    const response = await axios.put(url, {
      firstName: data.first,
      lastName: data.last,
      description: data.desc,
      hourlyRate: data.rate,
      areas: data.areas
      }
    );

    if (response.status !== 200) {
      throw new Error('Insert Coach Failed');
     }

    // Richiamo la mutation addCoach con il nuovo coach
    context.commit('addCoach', {
      ...newCoach,
      id: userId
    });
  },
  async loadCoaches(context, payload) {
    if (!payload.forceRefresh && !context.getters.shouldUpdate) {  // controllo se occorre fare una nuova lettura
      return;
    }
    const url = `${context.rootGetters.fireBaseUrl}/coaches.json`; // .json è richiesto da Firebase
    
    const response = await axios.get(url);
    const { data } = response;
    
    if (response.status !== 200) {
      throw new Error('Fetch Coach Failed');
    }

    const coaches = [];

    for(const key in data) {
      const coach = {
        id: key,
        firstName: data[key].firstName,
        lastName: data[key].lastName,
        description: data[key].description,
        hourlyRate: data[key].hourlyRate,
        areas: data[key].areas
      };
      coaches.push(coach); // elenco di tutte i coatch
    }

    context.commit('setCoaches', coaches);
    context.commit('setFetchTimeStamp'); // si salva la data dell'ultima lettura
  }
};
