export default {
  coaches(state) {
    return state.coaches;
  },
  hasCoaches(state) {
      return state.coaches && state.coaches.length > 0;
  },
  isCoach(state, getters, rootState, rootGetters) {
    const coaches = getters.coaches;
    const userId = rootGetters.userId;
    return coaches.some(coach => coach.id === userId);
  },
  shouldUpdate(state) {
    const lastFetch = state.lastFetch;
    if (!lastFetch) { // se non si è fatta una lettura si deve aggiornare
      return true;
    }
    const currentTimeStamp = new Date().getTime();
    // Se dall'ultima lettura è passato più di un minuto occorre aggiornare
    return (currentTimeStamp - lastFetch) / 1000 > 60;
  }
};
