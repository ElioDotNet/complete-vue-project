import axios from 'axios';

let timer;

export default {
  async login(context, payload) {
    return context.dispatch('auth', {
      ...payload,
      mode: 'login'
    });
  },
  async signup(context, payload) {
    return context.dispatch('auth', {
      ...payload,
      mode: 'signup'
    });
  },
  logout(context) {
    // Si rimuovono i dati nel localstorage
    localStorage.removeItem('token');
    localStorage.removeItem('userId');
    localStorage.removeItem('tokenExpiration');

    clearTimeout(timer); // si azzera il timer dopo avere fatto una logout
    // Per fare la logout basta richiamare la mutation setUser e impostare tutti i valori a null
    context.commit('setUser', {
      userId: null,
      token: null
    });
  },
  async auth(context, payload) {
    const mode = payload.mode; // indica se si sta facendo un login o signup
    let url = 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyBGmUnUTOc33D4t9qIPuqefPOE4vHJmgpI';
    
    if (mode === 'signup') {
      url = 'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyBGmUnUTOc33D4t9qIPuqefPOE4vHJmgpI';
    }
    
    const response = await axios.post(url, {
      email: payload.email,
      password: payload.password,
      returnSecureToken: true
    });
    
    if (response.status !== 200) {
      const error = new Error(
        response.data.message || 'Failed to authenticate. Check your login data.'
      );
      throw error;
    }

    const { idToken, localId, expiresIn  } = response.data;

    const expiration = +expiresIn * 1000; // scadenza token in millisecondi
    const expirationDate = new Date().getTime() + expiration; // data di scadenza del token

    // Si salvano i dati nel localstorage
    localStorage.setItem('token', idToken);
    localStorage.setItem('userId', localId);
    localStorage.setItem('tokenExpiration', expirationDate);

    // Dopo che il token è scaduto si fa un logout automatico
    timer = setTimeout(() => {
      context.dispatch('autoLogout');
    }, expiration);

    if (response.status !== 200) {
      throw new Error('Failed to authenticate');
    }

    context.commit('setUser', {
      token: idToken, // token relativo all'autenticazione
      userId: localId,
      tokenExpiration: expirationDate
    })
  },
  autoLogin(context) {
    // Si recuperano i dati nel localstorage
    const token = localStorage.getItem('token');
    const userId = localStorage.getItem('userId');
    const tokenExpiration = localStorage.getItem('tokenExpiration');

    const expiration = +tokenExpiration - new Date().getTime(); // indica il tempo rimanente per la scadenza token

    if (expiration < 0) { // il token è scaduto non si fa una login
      return
    }

     // Dopo che il token è scaduto si fa un logout automatico
     timer = setTimeout(() => {
      context.dispatch('autoLogout');
    }, expiration);

    if (token && userId) {
      context.commit('setUser', {
        token,
        userId
      });
    }
  },
  autoLogout(context) {
    context.dispatch('logout');
    context.commit('setAutoLogout');
  }
};
