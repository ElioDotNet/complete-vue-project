export default {
  userId(state) {
    return state.userId;
  },
  token(state) {
    return state.token;
  },
  isAuthenticated(state) {
    return !!state.token; // si è utenticaticati quando si ha un token
  },
  didAutoLogout(state) {
    return state.didAutoLogout;
  },
  fireBaseUrl() {
    // Url specifico per accedere a Firebase
    return 'https://vue-complete-project-default-rtdb.firebaseio.com';
  }
};
