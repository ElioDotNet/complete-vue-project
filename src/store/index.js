import { createStore } from 'vuex';
// Moduli usati nel progetto
import coachesModule from './modules/coaches/index';
import requestsModule from './modules/requests/index';
import authModule from './modules/auth/index';

const store = createStore({
  modules: {
    coaches: coachesModule, // chiave con cui si identifica il modulo dei coaches
    requests: requestsModule, // chiave con cui si identifica il modulo delle requests
    auth: authModule // chiave con cui si identifica il modulo delle autorizzazioni
  }
});

export default store;